package program.Model;
import java.util.regex.Pattern;

/**
 * A class that represents a Contact that can be added
 */
public class Model {

    private String firstName;
    private String lastName;
    private String phoneNumber;

    private final Pattern firstNamePattern = Pattern.compile("[A-Z][a-z]+");
    private final Pattern lastNamePattern = Pattern.compile("[A-Z][a-z]+(-[A-Z][a-z]+)?");
    private final Pattern phonePattern = Pattern.compile("\\+[1-9](\\s*-*[0-9]){11}");

    private boolean fieldCorrect(String input, Pattern pattern) {
        return pattern.matcher(input).matches();
    }

    private String formatPhone(String phone) {
        String oddPhoneRegEx = "\\s*-*";
        return phone.replaceAll(oddPhoneRegEx, "");
    }

    /**
     *
     * @param text That user try to input
     * @return if added successfully
     */
    public boolean setFirstName(String text) {
        if (!fieldCorrect(text, firstNamePattern)) return false;
        firstName = text;
        return true;
    }

    /**
     *
     * @param text That user try to input
     * @return if added successfully
     */
    public boolean setLastName(String text) {
        if (!fieldCorrect(text, lastNamePattern)) return false;
        lastName = text;
        return true;
    }

    /**
     * Sets formatted phone number
     * @param text That user try to input
     * @return if added successfully
     */
    public boolean setPhoneNumber(String text) {
        if (!fieldCorrect(text, phonePattern)) return false;
        phoneNumber = formatPhone(text);
        return true;
    }

    /**
     *
     * @return user's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @return user's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @return user's phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }
}
