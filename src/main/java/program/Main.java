package program;

import program.Controller.Controller;
import program.Model.Model;
import program.View.View;

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller(new Model(), new View());
        controller.addContact();
    }
}
