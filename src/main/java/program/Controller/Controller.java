package program.Controller;
import program.Model.Model;
import program.View.View;
import program.View.ViewTemplates;

import java.util.Scanner;


/**
* A class repesenting <strong>Controller</strong>.
*
* @author Dmytro Mykhalko
* @version 1.0
* */
public class Controller {
    private final Model model;
    private final View view;
    private final Scanner sc = new Scanner(System.in);

    public Controller(Model m, View v) {
        model = m;
        view = v;
    }

    /**
     * Creates a new contact in the ContactBook
     */
    public void addContact() {
        view.printMessage(ViewTemplates.INTRO_MESSAGE);

        view.printMessage(ViewTemplates.FIRST_NAME_LABEL);
        while (true) {
            if (!sc.hasNextLine()) continue;

            if (!model.setFirstName(sc.next()))
                view.printMessage(ViewTemplates.INVALID_NAME, ViewTemplates.TRY_AGAIN);
            else break;
        }

        view.printMessage(ViewTemplates.LAST_NAME_LABEL);
        while (true) {
            if (!sc.hasNextLine()) continue;

            if (!model.setLastName(sc.next()))
                view.printMessage(ViewTemplates.INVALID_NAME, ViewTemplates.TRY_AGAIN);
            else break;
        }

        view.printMessage(ViewTemplates.PHONE_LABEL);
        while (true) {
            if (!sc.hasNextLine()) continue;

            if (!model.setPhoneNumber(sc.next()))
                view.printMessage(ViewTemplates.INVALID_PHONE, ViewTemplates.TRY_AGAIN);
            else break;
        }

        view.printMessage(ViewTemplates.SUCCESS_MESSAGE);
        view.printMessage(model.getFirstName(), model.getLastName(), model.getPhoneNumber());
    }
}
