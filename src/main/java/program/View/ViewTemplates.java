package program.View;

/**
 * A class that represents constant Strings
 */
public class ViewTemplates {
    public final static String INTRO_MESSAGE = "Adding a new contact...";
    public final static String FIRST_NAME_LABEL = "First name: ";
    public final static String LAST_NAME_LABEL = "Last name: ";
    public final static String INVALID_NAME = "Wrong name format!";
    public final static String PHONE_LABEL = "Phone number: ";
    public final static String INVALID_PHONE = "Wrong phone format! Correct format: +XXXXXXXXXXX";
    public final static String TRY_AGAIN = "Try again.";
    public final static String SUCCESS_MESSAGE = "A new contact was added successfuly!";
    public final static String SPACE = " ";
}
