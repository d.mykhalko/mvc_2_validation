package program.View;

/**
 * Represents a view of proccess
 */
public class View {

    /**
     * prints a sequence of strings
     * @param str1 string thagt should be 100%
     * @param strings other strings to display
     */
    public void printMessage(String str1, String... strings) {
        StringBuilder result = new StringBuilder(str1);
        for (String str: strings) {
            result.append(ViewTemplates.SPACE).append(str);
        }
        System.out.println(result);
    }

}
